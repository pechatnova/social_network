import React from 'react';
import s from './Header.module.css';
import {NavLink} from "react-router-dom";

const Header = (props) => {
    return(
        <header className={s.header}>
            <img src="https://mk0dunhakdisdem7bjhx.kinstacdn.com/wp-content/themes/thrive-nouveau/logo.svg" alt="Thrive Social Network Demo" width="120px" alt="logo"/>

            <div className={s.loginBlock}>
                { props.isAuth ? props.login
                    : <NavLink to={'/login'}>Login</NavLink> }
            </div>
        </header>
    )
}

export default Header;