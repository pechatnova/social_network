import React from 'react';
import s from './Navbar.module.css';
import {NavLink} from "react-router-dom";

const Navbar = () => {
    return(
        <nav className={s.nav}>
            <div className="menu">
                <div className={s.item}>
                    <NavLink to="/profile" activeClassName={s.active}>Profile</NavLink>
                </div>
                <div className={`${s.item} ${s.active}`}>
                    <NavLink to="/dialogs" activeClassName={s.active}>Messages</NavLink>
                </div>
                <div className={s.item}>
                    <NavLink to="/users" activeClassName={s.active}>Users</NavLink>
                </div>
                <div className={s.item}>
                    <NavLink to="/news" activeClassName={s.active}>News</NavLink>
                </div>
                <div className={s.item}>
                    <NavLink to="/music" activeClassName={s.active}>Music</NavLink>
                </div>
                <div className={s.item}>
                    <NavLink to="/settings" activeClassName={s.active}>Settings</NavLink>
                </div>
            </div>

            <div className="friends">
                <div className="title">Friends</div>
                <div className="blockFriends">
                    <a href="#">
                        <img src="https://yt3.ggpht.com/a/AATXAJyn3Oj1n90vIOfDkoeuuqGKaYKqRPaW17VDkQ=s900-c-k-c0xffffffff-no-rj-mo" alt=""/>
                        <span>Masha</span>
                    </a>
                </div>
            </div>
        </nav>
    )
}

export default Navbar;