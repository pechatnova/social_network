import React from "react";
import s from './ProfileInfo.module.css';
import Preloader from "../../common/Preloader/preloader";
import ProfileStatus from "./ProfileStatus";


const ProfileInfo = (props) => {
    if (!props.profile) {
        return <Preloader />
    }


    return (
        <div>
            <div className={s.descriptionBlock}>
                <img src={props.profile.photos.large} />
                <ProfileStatus status={props.status} updateStatus={props.updateStatus} />
                <div>{props.profile.fullName}</div>
                <div>{props.profile.aboutMe}</div>
                <div>
                    <span>{props.profile.contacts.facebook}</span><br/>
                    <span>{props.profile.contacts.website}</span><br/>
                    <span>{props.profile.contacts.vk}</span><br/>
                    <span>{props.profile.contacts.twitter}</span><br/>
                    <span>{props.profile.contacts.instagram}</span><br/>
                    <span>{props.profile.contacts.youtube}</span><br/>
                    <span>{props.profile.contacts.github}</span><br/>
                    <span>{props.profile.contacts.mainLink}</span><br/>
                </div>
                <div>{props.profile.lookingForAJob}</div>
                <div>{props.profile.lookingForAJobDescription}</div>
                <div>   <img src={props.profile.photos.small} /></div>
            </div>
        </div>
    )
}

export default ProfileInfo;