import * as axios from "axios";


const instance = axios.create({
    baseURL: "https://social-network.samuraijs.com/api/1.0/",
    withCredentials: true,
    headers: {
        "API-KEY": "8ace0d82-a77f-4f22-84f9-631e76de9536"
    }
});

export const userAPI ={
    getUsers (currentPage = 1, pageSize = 100) {
        return instance.get(`users?page=${currentPage}&count=${pageSize}`)
            .then(response => response.data);
    },
    follow (userId) {

        return instance.post(`follow/${userId}`)
            .then(response => response.data);
    },
    unfollow (userId) {
        return instance.delete(`follow/${userId}`)
            .then(response => response.data);
    },
    auth () {
        return instance.get(`auth/me`)
            .then(response => response.data);
    }
}


export const profileAPI ={
    getProfile(userId) {
        return  instance.get(`profile/` + userId);
    },
    getStatus(userId) {
        return  instance.get(`profile/status/` + userId);
    },
    updateStatus(status) {
        return  instance.put(`profile/status`, { status: status });
    }
}

export const authAPI ={
    me () {
        return instance.get(`auth/me`)
            .then(response => response.data);
    }
}




